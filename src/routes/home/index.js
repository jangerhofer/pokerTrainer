import { h } from 'preact';
import styled from 'styled-components'

import {Card, Hand, Deck} from '../../components/util'

import CardDisplay from '../../components/Card'

const Container = styled.div`
	height: 100%;
	width: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
`

const CardContainer = styled.div`
	margin: 5%;
`

const NewDeck = new Deck()
NewDeck.shuffle()

const Home = () => (
	<Container>
		{
			NewDeck.deal(2).map(card => {
				return (
					<CardContainer>
				<CardDisplay rank={card._normRank} suit={card._normSuit} scale={4}/>
				</CardContainer>
			)}
		)
		}
	</Container>
);

export default Home;
