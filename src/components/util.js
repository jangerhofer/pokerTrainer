import _ from 'lodash'

const suits = ['Hearts', 'Spades', 'Clubs', 'Diamonds'];
const ranks = ['Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King'];

class Card {
    constructor({suitIndex, rankIndex}) {
        this.suitIndex = suitIndex
        this.rankIndex = rankIndex

        this._normSuit = suits[this.suitIndex].toLowerCase().slice(0,-1)
        switch(this.rankIndex) {
            case 10:
                this._normRank = "jack"
                break;
            case 11:
                this._normRank = "queen"
                break;
            case 12:
                this._normRank = "king"
                break;
            default:
                this._normRank = (rankIndex+1).toString()
        }
    }
    
    toString() {
        return `${ranks[this.rankIndex]} of ${suits[this.suitIndex]}`
    }
}

class Deck {
    constructor() {
      this.deck = [];
  
      _.range(4).map(suitIndex => {
        _.range(13).map(rankIndex => {
            this.deck.push(new Card({suitIndex, rankIndex}));
          })
      })
        
    }

    toString() {
        return this.deck.map(card => card.toString())
    }

    shuffle() {
        const { deck } = this;
        let m = deck.length, i;
      
        while (m) {
          i = Math.floor(Math.random() * m--);
      
          [deck[m], deck[i]] = [deck[i], deck[m]];
        }
      
        return this;
      }

      deal(numCards = 1) {
          const dealtCards = this.deck.slice(0, numCards)
          this.deck = this.deck.slice(numCards)
          return dealtCards
      }
  }

  class Hand {
      constructor({cards} = {}) {
          this.cards = cards
      }
      
      addCards(cardsList) {
          this.cards.concat(cardsList)
          this.sortHand()
      }

      sortHand() {
          this.cards = _.sortBy(this.cards, (card) => card.valueIndex)
      }
  }

export {suits, ranks, Card, Deck, Hand}